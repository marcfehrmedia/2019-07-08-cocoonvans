const config = require('./src/utils/siteConfig')

const path = require(`path`)

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  const loadProjects = new Promise((resolve, reject) => {
    graphql(`
      {
        allContentfulPost(
          sort: { fields: [publishDate], order: DESC }
          limit: 1000
          filter: { node_locale: { eq: "de-CH" } }
        ) {
          edges {
            node {
              slug
              publishDate
              fahrzeug
              dauer
              kunde
              fertigstellung
              gallery {
                id
                title
                fluid {
                  srcSet
                  src
                  base64
                  tracedSVG
                  srcWebp
                  srcSetWebp
                }
              }
            }
          }
        }
      }
    `).then(result => {
      const projects = result.data.allContentfulPost.edges
      const projectsPerFirstPage = config.projectsPerHomePage
      const { projectsPerPage } = config
      const numPages = Math.ceil(
        projects.slice(projectsPerFirstPage).length / projectsPerPage
      )

      // Create main home page
      createPage({
        path: `/ausbau/projekte/`,
        component: path.resolve(`./src/templates/projects.js`),
        context: {
          limit: projectsPerFirstPage,
          skip: 0,
          numPages: numPages + 1,
          currentPage: 1,
        },
      })

      // Create additional pagination on home page if needed
      Array.from({ length: numPages }).forEach((_, i) => {
        createPage({
          path: `/ausbau/projekte/${i + 2}/`,
          component: path.resolve(`./src/templates/projects.js`),
          context: {
            limit: projectsPerPage,
            skip: i * projectsPerPage + projectsPerFirstPage,
            numPages: numPages + 1,
            currentPage: i + 2,
          },
        })
      })

      // Create each individual post
      projects.forEach((edge, i) => {
        const projectPrefix = `/ausbau/projekte/`
        const prev = i === 0 ? null : projects[i - 1].node
        const next = i === projects.length - 1 ? null : projects[i + 1].node
        createPage({
          path: `${projectPrefix}${edge.node.slug}/`,
          component: path.resolve(`./src/templates/project.js`),
          context: {
            slug: edge.node.slug,
            prev,
            next,
          },
        })
      })
      resolve()
    })
  })

  const loadTags = new Promise((resolve, reject) => {
    graphql(`
      {
        allContentfulTag {
          edges {
            node {
              slug
              post {
                id
              }
            }
          }
        }
      }
    `).then(result => {
      const tags = result.data.allContentfulTag.edges
      const { projectsPerPage } = config

      // Create tag pages with pagination if needed
      tags.map(({ node }) => {
        const totalProjects = node.post !== null ? node.post.length : 0
        const numPages = Math.ceil(totalProjects / projectsPerPage)
        Array.from({ length: numPages }).forEach((_, i) => {
          createPage({
            path:
              i === 0 ? `/ausbau/tag/${node.slug}/` : `/ausbau/tag/${node.slug}/${i + 1}/`,
            component: path.resolve(`./src/templates/tag.js`),
            context: {
              slug: node.slug,
              limit: projectsPerPage,
              skip: i * projectsPerPage,
              numPages,
              currentPage: i + 1,
            },
          })
        })
      })
      resolve()
    })
  })

  const loadPages = new Promise((resolve, reject) => {
    graphql(`
      {
        allContentfulPage {
          edges {
            node {
              slug
            }
          }
        }
      }
    `).then(result => {
      const pages = result.data.allContentfulPage.edges
      pages.map(({ node }) => {
        createPage({
          path: `/${node.slug}/`,
          component: path.resolve(`./src/templates/page-de.js`),
          context: {
            slug: node.slug,
          },
        })
      })
      resolve()
    })
  })

  const loadRentals = new Promise((resolve, reject) => {
    graphql(`
      {
        allContentfulRentalVan(filter: { node_locale: { eq: "de-CH" } }) {
          edges {
            node {
              id
              title
              slug
              description
              calendarID
              text {
                json
              }
              excerpt {
                json
              }
            }
          }
        }
      }
    `).then(result => {
      const rentalVans = result.data.allContentfulRentalVan.edges

      // Create each individual post
      rentalVans.forEach((edge, i) => {
        const projectPrefix = `/vermietung/vans/`
        createPage({
          path: `${projectPrefix}${edge.node.slug}/`,
          component: path.resolve(`./src/templates/rental.js`),
          context: {
            slug: edge.node.slug,
          },
        })
      })
      resolve()
    })
  })

  return Promise.all([loadProjects, loadTags, loadPages, loadRentals])
}

/*
,         Multi language snippet
          createPage({
            path: `${node.slug}/en`,
            component: path.resolve(`./src/templates/page-en.js`),
            context: {
              slug: node.slug,
            },
          })
 */
