import React from 'react'
import PropTypes from 'prop-types'
import axios from 'axios'
import styled from 'styled-components'
import moment from 'moment'
// import ReCAPTCHA from "react-google-recaptcha"
import deLocale from 'moment/locale/de'
import 'semantic-ui-css/components/icon.css'
import 'semantic-ui-css/components/message.css'

/*
  ⚠️ This is an example of a contact form powered with Netlify form handling.
  Be sure to review the Netlify documentation for more information:
  https://www.netlify.com/docs/form-handling/
*/

const SocialIcons = styled.div`
    margin-bottom: 2rem;
    padding-top: 2rem;
    width: 100%;
    text-align: center;
    .ui.basic.buttons .button, .ui.basic.button {
      width: auto;
      @media screen and (max-width: 550px) {
        width: 100%;
        float: left;
        display: block;
        margin-bottom: 0.35rem;
      }
    }
  `

const Form = styled.form`
  max-width: ${props => props.theme.sizes.maxWidthCentered};
  margin: 0 auto;
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: flex-start;
  input,
  textarea {
    font-family: inherit;
    font-size: inherit;
    border: none;
    outline: none;
    background: ${props => props.theme.colors.tertiary};
    color: ${props => props.theme.colors.base};
    border-radius: 2px;
    padding: 1em;
    &::-webkit-input-placeholder {
      color: gray;
    }
    &::-moz-placeholder {
      color: gray;
    }
    &:-ms-input-placeholder {
      color: gray;
    }
    &:-moz-placeholder {
      color: gray;
    }
    &:required {
      box-shadow: none;
    }
    &:focus {
      outline: none;
    }
  }
  &::before {
    content: '';
    background: black;
    height: 100%;
    width: 100%;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1;
    transition: 0.2s all;
    opacity: ${props => (props.overlay ? '.8' : '0')};
    visibility: ${props => (props.overlay ? 'visible' : 'hidden')};
  }
`

const FormTitle = styled.h2`
  width: 100%;
  display: block;
  font-size: 1.5em;
  line-height: 1.5;
  padding: 1rem 0;
`

const FormText = styled.p`
  width: 100%;
  display: block;
`

const Name = styled.input`
  margin: 0 0 1em 0;
  width: 100%;
  @media (min-width: ${props => props.theme.responsive.small}) {
    width: 49%;
  }
`

const Email = styled.input`
  margin: 0 0 1em 0;
  width: 100%;
  @media (min-width: ${props => props.theme.responsive.small}) {
    width: 49%;
  }
`

const Message = styled.textarea`
  width: 100%;
  margin: 0 0 1em 0;
  line-height: 1.6;
  min-height: 250px;
  resize: vertical;
`

const Submit = styled.input`
  background: ${props => props.theme.colors.base} !important;
  color: white !important;
  cursor: pointer;
  transition: 0.2s;
  &:hover {
    background: ${props => props.theme.colors.highlight} !important;
  }
`

const Modal = styled.div`
  background: white;
  padding: 2em;
  border-radius: 2px;
  position: fixed;
  min-width: 75%;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  margin: 0 auto;
  z-index: 99;
  display: flex;
  flex-flow: column;
  align-items: flex-start;
  transition: 0.2s all;
  opacity: ${props => (props.visible ? '1' : '0')};
  visibility: ${props => (props.visible ? 'visible' : 'hidden')};
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    min-width: inherit;
    max-width: 400px;
  }
  p {
    line-height: 1.6;
    margin: 0 0 2em 0;
  }
`

const Button = styled.div`
  background: ${props => props.theme.colors.base};
  font-size: 1em;
  display: inline-block;
  margin: 0 auto;
  border: none;
  outline: none;
  cursor: pointer;
  color: white;
  padding: 1em;
  border-radius: 2px;
  text-decoration: none;
  transition: 0.2s;
  z-index: 99;
  &:focus {
    outline: none;
  }
  &:hover {
    background: ${props => props.theme.colors.highlight};
  }
`

const encode = data => {
  return Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&')
}

class RentalForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      rentFrom: moment(this.props.startDate).format('L'),
      rentTo: moment(this.props.endDate).format('L'),
      duration: this.props.duration,
      name: '',
      email: '',
      message: '',
      phone: '',
      persons: '',
      showModal: false,
      messageSent: false,
      vanName: this.props.vanName || '',
    }
  }

  handleInputChange = event => {
    const target = event.target
    const value = target.value
    const name = target.name
    this.setState({
      [name]: value,
    })
  }

  handleSubmit = event => {
    fetch('/?no-cache=1', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: encode({
        'form-name': 'vermietung',
        'van-name': this.props.vanName,
        'rentFrom': this.state.rentFrom,
        'rentTo': this.state.rentTo,
        'duration': this.state.duration,
        'persons': this.state.persons,
        'name': this.state.name,
        'phone': this.state.phone,
        'email': this.state.email,
        'message': this.state.message,
        'subject': `Anfrage für ${this.props.vanName} (${this.props.duration} Tage) von ${this.state.name}`,
        'replyTo': this.state.email,
      })
    })
      .then(this.handleSuccess)
      .catch(error => alert(error))

    /*console.log('second fetch')
    axios.post("/rental", JSON.stringify(this.state)).then(response => {
      if (response.status !== 200) {
        this.handleError()
      } else {
        this.handleSuccess()
      }
    })*/

    event.preventDefault()
  }

  handleSuccess = () => {
    this.setState({
      rentFrom: moment(this.props.startDate).format('L') || moment(),
      rentTo: moment(this.props.endDate).format('L') || moment(),
      duration: '',
      name: '',
      phone: '',
      email: '',
      message: '',
      persons: '',
      vanName: '',
      messageSent: true,
      showModal: true,
    })
  }

  closeModal = () => {
    this.setState({ showModal: false })
  }

  render() {
    moment.locale('de', deLocale)

    return (
      <Form
        name="vermietung"
        onSubmit={this.handleSubmit}
        data-netlify-honeypot="bot"
        netlify-honeypot="bot"
        data-netlify="true"
        overlay={this.state.showModal}
        onClick={this.closeModal}
      >
        <input type="hidden" id="subject" name="subject" value={`Anfrage für ${this.props.vanName} (${this.props.duration} Tage) von ${this.state.name}`} />
        {/*<input type="hidden" name="replyto" value={this.state.email} />*/}
        <input type="hidden" name="form-name" value="vermietung" />
        <input type="hidden" name="van-name" value={this.state.vanName} />
        <input type="hidden" name="duration" value={this.props.duration + ' days' || ''} />
        <p hidden>
          <label>
            Don’t fill this out:{' '}
            <input name="bot" onChange={this.handleInputChange} />
          </label>
        </p>

        <FormTitle>Mietdauer ({this.props.duration} {this.props.duration === 1 ? 'Tag' : 'Tage'})</FormTitle>

        <Name
          name="rentFrom"
          type="text"
          disabled
          placeholder="Bis"
          value={`Abholung: ${moment(this.props.startDate).format('L')}`}
          required
        />

        <Name
          name="rentTo"
          type="text"
          disabled
          placeholder="Bis"
          value={`Rückgabe: ${moment(this.props.endDate).format('L')}`}
          required
        />

        <FormTitle>
          Details für deine Anfrage
        </FormTitle>

        <Name
          name="name"
          type="text"
          placeholder="Dein Name"
          value={this.state.name}
          onChange={this.handleInputChange}
          required
        />

        <Name
          name="phone"
          type="tel"
          placeholder="Telefon / Whatsapp"
          value={this.state.phone}
          onChange={this.handleInputChange}
          required
        />

        <Name
          name="persons"
          type="number"
          placeholder="Anzahl Personen"
          value={this.state.persons}
          pattern="[0-9]*"
          onChange={this.handleInputChange}
          required
        />

        <Email
          name="email"
          type="email"
          placeholder="E-Mail Adresse"
          value={this.state.email}
          onChange={this.handleInputChange}
          required
        />

        <div className="ui icon info message" style={{ borderRadius: 0 }}>
          <i className="icon question" />
          Wir sorgen uns sehr gut um unsere Camper – gib uns deshalb bitte etwas mehr Informationen zu eurem geplanten Abenteuer an:
        </div>

        <Message
          name="message"
          type="text"
          placeholder={`Für was möchtet ihr den Van mieten? Wo gehts hin? Wie lange?`}
          value={this.state.message}
          onChange={this.handleInputChange}
          required
        />

        <Submit name="submit" type="submit" value="Anfrage senden" disabled={this.props.disabled} />

        <Modal visible={this.state.showModal}>
          <p>
            Vielen Dank für deine Anfrage. Wir werden uns so schnell wie möglich bei dir melden.
          </p>
          <Button onClick={this.closeModal}>Cool</Button>
        </Modal>
      </Form>
    )
  }
}

RentalForm.propTypes = {
  startDate: PropTypes.object,
  endDate: PropTypes.object,
  duration: PropTypes.number,
  disabled: PropTypes.bool
}

export default RentalForm