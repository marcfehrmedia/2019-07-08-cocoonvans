import React, { useState, useEffect } from 'react'

export default function CalendarLoader(props) {
  const [data, setData] = useState([])

  useEffect(() => {
    fetch(
      'http://www.google.com/calendar/feeds/{calendarId}@group.calendar.google.com/public/free-busy?orderby=starttime&sortorder=ascending&futureevents=true&alt=json\n'
    )
      .then(response => response.json())
      .then(data => setData(data))
  })

  return (
    <div>
      <ul>
        {data.map(el => (
          <li key={el.id}>{el.title}</li>
        ))}
      </ul>
    </div>
  )
}
