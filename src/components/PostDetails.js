import React from 'react'
import styled from 'styled-components'

import 'semantic-ui-css/components/table.min.css'

const Wrapper = styled.div`
  margin: 2rem auto 3rem;
  max-width: ${props => props.theme.sizes.maxWidthCentered};
`

const Date = styled.p`
  display: inline-block;
`

const ReadingTime = styled.p`
  display: inline-block;
`

const PostDetails = props => {
  return (
    <Wrapper>
      <table className='ui celled table'>
        <thead>
          <tr>
            <th colSPan={2}>
              <h2>Fahrzeug-Details</h2>
            </th>
          </tr>
        </thead>
        <tbody>
          {props.vehicle && (
            <tr>
              <td>Fahrzeug</td>
              <td>{props.vehicle}</td>
            </tr>
          )}
          {props.doneBy && (
            <tr>
              <td>Fertigstellung:</td>
              <td>{props.doneBy}</td>
            </tr>
          )}
          {props.dauer && (
            <tr>
              <td>Umsetzungsdauer:</td>
              <td>{props.dauer}</td>
            </tr>
          )}
          {props.customer && (
            <tr>
              <td>Kunde:</td>
              <td>
                <a
                  href={`https://www.instagram.com/${props.customer}`}
                  target='_blank'
                  rel='noopener'
                >
                  {props.customer !== ' ' && (
                    <i className='ui icon instagram' />
                  )}
                  {props.customer}
                </a>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </Wrapper>
  )
}

export default PostDetails
