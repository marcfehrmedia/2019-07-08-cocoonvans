import React from 'react'
import styled from 'styled-components'
import 'semantic-ui-css/components/icon.css'
import { Link } from 'gatsby'

const Wrapper = styled.footer`
  display: flex;
  z-index: 999;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: flex-start;
  position: relative;
  margin: 0 auto;
  width: 100%;
  background: #282828;
  -webkit-box-shadow: 0px -5px 72px -19px rgba(0, 0, 0, 0.9);
  -moz-box-shadow: 0px -5px 72px -19px rgba(0, 0, 0, 0.9);
  box-shadow: 0px -5px 72px -19px rgba(0, 0, 0, 0.9);
`

const List = styled.ul`
  max-width: ${props => props.theme.sizes.maxWidth};
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  // border-top: 1px solid ${props => props.theme.colors.secondary};
  padding: 1rem 2rem;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    padding: 2rem 3rem;
  }
  margin: 0 auto;
`

const Item = styled.li`
  display: inline-block;
  padding: 0.25em 0;
  width: 100%;
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    width: auto;
  }
  a,
  a span {
    font-weight: 700;
    transition: all 0.2s;
    border: none;
    color: ${props => props.theme.colors.inverted};
    &:visited {
      color: ${props => props.theme.colors.inverted};
    }
    &:hover {
      color: ${props => props.theme.colors.highlight};
    }
  }
`

const Footer = () => (
  <Wrapper>
    <List>
      <Item>
        <Link to='/about/'>
          <i className={'ui icon copyright outline'} />
          {`${new Date().getFullYear()} Cocoon Vans`}
        </Link>
      </Item>
      <Item>
        <Link to='/datenschutz/'>
          <i className={'ui icon eye'} />
          Datenschutzrichtlinien
        </Link>
      </Item>
      <Item>
        <a
          href='https://www.instagram.com/cocoonvans/'
          rel='nofollow noopener noreferrer'
          target='_blank'
        >
          <span>
            <i className={'ui icon instagram'} />
            cocoonvans
          </span>
        </a>
      </Item>
    </List>
  </Wrapper>
)

export default Footer
