import React from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'

import HeaderLogoImg from '../assets/1x/logo-composed.png'

import DesktopTopNavComponent from './TopNav.Desktop'
import MobileTopNavComponent from './TopNav.Mobile'

// import MobileTopNavComponent from './MenuMobile'
// import { elastic as BurgerMenu } from 'react-burger-menu'

import config from '../utils/siteConfig'
import '../styles/animations.scss'

const MobileNav = styled.nav`
  display: none;
  @media screen and (max-width: ${props => props.theme.responsive.medium}) {
    display: block;
  }
  
  .hidden {
    pointer-events: none;
    display: none;
  }
  
  /* Position and sizing of burger button */
  .bm-burger-button {
    position: fixed;
    width: 36px;
    height: 30px;
    right: 1.8rem;
    top: 1.8rem;
  }

  /* Color/shape of burger icon bars */
  .bm-burger-bars {
    background: white;
  }

  /* Color/shape of burger icon bars on hover*/
  .bm-burger-bars-hover {
    background: ${props => props.theme.colors.tertiary};
  }

  /* Position and sizing of clickable cross button */
  .bm-cross-button {
    height: 24px;
    width: 24px;
  }

  /* Color/shape of close button cross */
  .bm-cross {
    background: #bdc3c7;
  }

  /*
  Sidebar wrapper styles
  Note: Beware of modifying this element as it can break the animations - you should not need to touch it in most cases
  */
  .bm-menu-wrap {
    position: fixed;
    height: 100%;
    width: auto;
    margin-top: -1.5em;
  }

  /* General sidebar styles */
  .bm-menu {
    background: ${props => props.theme.colors.base};
    padding: 2.5em 1.5em 0;
    font-size: 2em;
    a,
    a:visited,
    a:focus {
      -webkit-box-shadow: none;
      -moz-box-shadow: none;
      box-shadow: none;
      margin: 1rem auto;
      color: ${props => props.theme.colors.highlight};
      font-family: ${props => props.theme.fonts.title};
    }
  }

  /* Morph shape necessary with bubble or elastic */
  .bm-morph-shape {
    fill: ${props => props.theme.colors.base};
  }

  .bm-morph-shape {
    width: unset !important;
    // background: ${props => props.theme.colors.highlight};
  }

  /* Wrapper for item list */
  .bm-item-list {
    color: ${props => props.theme.colors.secondary};
    padding: 0.8em;
    width: auto;
    display: block;
    padding: 0;
    margin: 0 auto;
  }

  /* Individual item */
  .bm-item {
    display: inline-block;
    outline: none;
    width: 100%;
    &.shop {
      margin-bottom: 2rem;
      // padding-bottom: 2rem;
      // border-bottom: 2px solid ${props => props.theme.colors.highlight};
    }
    &.hidden {
      display: none !important;
    }
  }

  /* Styling of overlay */
  .bm-overlay {
    top: 0;
    background: ${props => props.theme.colors.base};
  }
`

const Nav = styled.nav`
  z-index: 99;
  width: 100%;
  max-width: ${props => props.theme.sizes.maxWidth};
  margin: 0 auto;
  padding: 0.5rem 1rem;

  ul {
    display: flex;
    justify-content: space-between;
  }

  li {
    z-index: 10;
    font-family: ${props => props.theme.fonts.text};
    display: inline-block;
    margin-left: 1rem;
    font-size: 1.2rem;
    text-transform: uppercase;
    font-family: 'Oswald', sans-serif;
    @media screen and (max-width: ${props => props.theme.responsive.medium}) {
      display: none;
      margin-left: 0.5rem;
      font-size: 85%;
    }

    &.shop {
      position: relative;
      margin: 0;
      flex-basis: 10%;

      @media screen and (max-width: ${props => props.theme.responsive.large}) {
        opacity: 0;
      }
    }

    &.vermietung {
      // opacity: 0;
      // pointer-events: none;
      position: relative;
      margin: 0;
      flex-basis: 100%;

      @media screen and (max-width: ${props => props.theme.responsive.large}) {
        // opacity: 0;
      }
    }

    &:first-child {
      position: relative;
      margin: 0;
      flex-basis: 12%;

      @media screen and (max-width: ${props => props.theme.responsive.large}) {
        opacity: 0;
      }
    }
  }

  a {
    text-decoration: uppercase;
    color: ${props => props.theme.colors.inverted};
    font-weight: 700;
    transition: all 0.2s;
    &:hover {
      color: ${props => props.theme.colors.highlight};
    }
  }
`

const Header = styled.header`
  background: ${props => props.theme.colors.base};
  width: 100%;
  padding: 1rem 0.75rem;
  -webkit-box-shadow: 0px 5px 72px -19px rgba(254, 194, 64, 0.61);
  -moz-box-shadow: 0px 5px 72px -19px rgba(254, 194, 64, 0.61);
  box-shadow: 0px 5px 72px -19px rgba(254, 194, 64, 0.61);
`

const activeLinkStyle = {
  color: '#FEC340'
  // borderBottom: '2px solid #FEC340'
}

const activeBurgerLinkStyle = {
  pointerEvents: 'none',
  color: '#ddd',
  display: 'none'
}

const HeaderLogo = styled.div`
  width: 100%;
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  // margin-top: 0.25rem;
  height: auto;
  text-align: center;
  z-index: 0;
  top: 0;
  left: 0;

  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    top: 0;
  }

  a {
    display: flex;
    color ${props => props.theme.colors.inverted};
  }

  h1 {
    font-size: 2rem;
    color ${props => props.theme.colors.highlight};
    display: inline-flex;
    align-self: center;
    margin-left: 1rem;
    
    @media screen and (max-width: ${props => props.theme.responsive.large}) {
      font-size: 1.5rem;
    }
    
    @media screen and (max-width: ${props =>
      props.theme.responsive.mediumLarge}) {
      display: none;
    }
    
    @media screen and (max-width: ${props =>
      props.theme.responsive.smallMedium}) {
      display: block;
    }
    
  }

  img {
    display: inline-flex;
    align-self: center;
    height: 54px;
    width: 54px;
    background: transparent;
    object-fit: cover;
    
    &.heartbeat-hover:hover {
      -webkit-animation: heartbeat 1.5s ease-in-out 1 both;
      animation: heartbeat 1.5s ease-in-out 1 both;
    }
    @media screen and (min-width: ${props => props.theme.responsive.medium}) {
      width: 72px;
      height: 72px;;
    }
  }
`

const Menu = () => {
  const mappedLinks = config.menuLinks.map((el, i) => (
    <li key={`menu-link-${i}`}>{el.title}</li>
  ))

  return (
    <Header>
      <DesktopTopNavComponent key='topnav-desktop' links={config.menuLinks} />
      <MobileTopNavComponent key='topnav-mobile' links={config.menuLinks} />
      <HeaderLogo>
        <Link to='/'>
          <img className='heartbeat-hover' src={HeaderLogoImg} />
          <h1>Cocoon Vans</h1>
        </Link>
      </HeaderLogo>
    </Header>
  )
}

export default Menu
