import React from 'react'
import Img from 'gatsby-image/withIEPolyfill'
import Carousel from '@brainhubeu/react-carousel'
import styled from 'styled-components'
import '../styles/slider.scss'

const StyledDiv = styled.div`
  .label {
    font-size: 0.9rem;
    color: #262626;
    padding: 0.25rem;
    font-style: italic;
  }
  .BrainhubCarousel {
    position: relative;
    display: grid;
    // overflow-y: visible;
    padding: 2rem 0 2rem 0;
    h1,
    h2,
    h3 {
      font-size: 1.1em;
    }
    p {
      font-size: 1em;
    }
    .BrainhubCarousel__arrows {
      position: absolute;
      z-index: 9999;
      top: calc(50% - 20px);
      background: ${props => props.theme.colors.secondary};
      &:hover {
        background: ${props => props.theme.colors.highlight};
      }
      &.BrainhubCarousel__arrowLeft {
        span {
          max-width: 20px;
          max-height: 20px;
        }
      }
      &.BrainhubCarousel__arrowRight {
        right: 0;
        span {
          max-width: 20px;
          max-height: 20px;
        }
      }
    }

    .BrainhubCarousel__track {
      li {
        width: 100%;
        display: block;
        padding: 0;
        margin: 0;
        left: 0; right: 0;
        top: 0; bottom: 0;
        text-align: center;
      }
    }

    .BrainhubCarousel__trackContainer {
      overflow: hidden;
    }}
  }
`

const ImageWrapper = styled.a`
  display: flex;
  position: relative;
  border-radius: 0.25rem;
  align-items: center;
  justify-content: center;
  flex-direction: row;

  @media screen and (max-width: 500px) {
    flex-direction: column;
  }

  padding: 1rem;

  p {
    padding: 4rem;

    @media screen and (max-width: 500px) {
      padding: 1rem 0;
    }
  }

  a {
    color: white;
    &:active, &focus {
      color: white;
    }
    
    font-weight: 700;
  }
`

const AdSlider = props => {
  const mappedImages = props.data.map((el, i) => (
    <ImageWrapper 
      key={`ad-image-wrapper-${i}`}
      style={{
        background: el.node.hintergrundfarbe || "transparent"
      }}
      href={ el.node.link || `mailto:${el.node.email}` }
    >
      { el.node.image &&
        <Img
          key={`preview-image-${i}`}
          fluid={el.node.image.fluid}
          objectFit="contain"
          objectPosition="50% 50%"
          style={{
            height: "15vh",
            width: "100%"
            
          }}
          imgStyle={{
            height: '100%',
            objectFit: 'contain',
            objectPosition: '50% 50%',
          }}
        />
      }
      { el.node.email && !el.node.link &&
      <p 
       style={{color: el.node.textfarbe || 'white'}}
      >
        { el.node.slogan ? ( el.node.slogan ) : ("Interesse geweckt? Jetzt mehr erfahren:")}<br /><a href={`mailto:${el.node.email}`}>{ el.node.email }</a>
      </p>
      }
      { !el.node.email && el.node.link &&
      <p 
       style={{color: el.node.textfarbe || 'white'}}
      >
        Interesse geweckt? Jetzt mehr erfahren: <a href={`${el.node.link}}`}><i className="icon external link" />{ el.node.link }</a>
      </p>
      }
    </ImageWrapper>
  ))

  return (
    <StyledDiv>
      <p className="label">Werbung</p>
      <Carousel
        // autoPlay={props.data.length > 1 ? 6000 : "null"}
        // animationSpeed={8000}
        infinite
        centered
        // arrows
        // indicators
        stopAutoPlayOnHover
        slidesPerPage={props.slidesPerPage || 1}
        slidesPerScroll={1}
        breakpoints={{
          950: {
            // these props will be applied when screen width is less than 1000px
            slidesPerPage: 1,
          },
          800: {
            // these props will be applied when screen width is less than 1000px
            slidesPerPage: 1,
          },
          650: {
            // these props will be applied when screen width is less than 1000px
            slidesPerPage: 1,
          },
          500: {
            slidesPerPage: 1,
          },
        }}
      >
        { mappedImages }
      </Carousel>
    </StyledDiv>
  )
}

export default AdSlider
