import React from 'react'
import styled from 'styled-components'
import { Link } from 'gatsby'
import CarouselSlider from './Slider.Carousel'

const Post = styled.li`
  position: relative;
  display: block;
  border-radius: 0;
  margin: 1rem auto 2rem auto;
  width: 100%;
  transition: background 0.2s;
  border-bottom: 1px solid ${props => props.theme.colors.highlight};
  box-shadow: 1px -1px 23px 0px rgba(0, 0, 0, 0.3);
  -webkit-box-shadow: 1px -1px 23px 0px rgba(0, 0, 0, 0.3);

  a {
    display: flex;
    flex-flow: column;
    width: 100%;
    color: ${props => props.theme.colors.base};
    text-decoration: none;
    .gatsby-image-wrapper {
      height: 0;
      padding-bottom: 60%;
      @media screen and (min-width: ${props => props.theme.responsive.small}) {
        padding-bottom: ${props => (props.featured ? '40%' : '60%')};
      }
    }
  }
  -moz-box-shadow: 1px -1px 23px 0px rgba(0, 0, 0, 0.3);

  &:hover {
    box-shadow: 1px -1px 23px 0px rgba(0, 0, 0, 0.5);
    -webkit-box-shadow: 1px -1px 23px 0px rgba(0, 0, 0, 0.5);
    -moz-box-shadow: 1px -1px 23px 0px rgba(0, 0, 0, 0.5);
  }
  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    border-bottom: none;
    flex: ${props => (props.featured ? '0 0 100%' : '0 0 49%')};
  }
  @media screen and (min-width: ${props => props.theme.responsive.medium}) {
    flex: ${props => (props.featured ? '0 0 100%' : '0 0 100%')};
  }
`

const Title = styled.h2`
  color: ${props => props.theme.colors.primary};
  font-size: 2rem;
  font-weight: 600;
  text-transform: uppercase;
  margin: 1rem 1rem 0.5rem 1rem;
`

const VehicleText = styled.h3`
  margin: 0 1rem 0 1rem;
  font-size: 1.5rem;
  color: gray;
`

const Card = ({
  slug,
  heroImage,
  title,
  description,
  featured,
  text,
  gallery,
  excerpt,
  ...props
}) => {
  return (
    <Post featured={featured}>
      {gallery && <CarouselSlider images={gallery} />}
      <Link to={`/vermietung/vans/${slug}/`}>
        <Title>{title}</Title>
        <VehicleText>{description}</VehicleText>
        <div style={{ margin: '1rem' }}>
          <button className='ui fluid icon button'>
            <i className='icon calendar outline' />{' '}
            <span style={{ fontFamily: 'Roboto' }}>Jetzt buchen</span>
          </button>
        </div>
      </Link>
    </Post>
  )
}

export default Card
