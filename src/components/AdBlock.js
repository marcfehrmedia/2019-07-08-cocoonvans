import React from 'react'
import { StaticQuery, graphql } from 'gatsby'

import styled from 'styled-components'
import AdSlider from './Slider.Ads'

const AdContainer = styled.div`
  width: 100%;
  height: auto;
  margin: 1rem auto;
  display: flex;
  flex-direction: row;
  justify-content: center;

  @media screen and (max-width: 500px) {
    position: relative;
    display: flex;
  }
`

export default function AdBlock(props) {
  return (
    <StaticQuery
      query={graphql`
        query AdQuery {
          allContentfulWerbung(filter: { node_locale: { eq: "de-CH" } }) {
            edges {
              node {
                id
                email
                slogan
                hintergrundfarbe
                image {
                  fluid {
                    srcWebp
                    srcSetWebp
                    srcSet
                    src
                    base64
                  }
                  description
                }
                title
                text {
                  json
                }
              }
            }
          }
        }
      `}
      render={data => {
        return (
          <AdContainer
            style={{ background: 'transparent', textAlign: 'center' }}
          >
            <AdSlider data={data.allContentfulWerbung.edges} />
          </AdContainer>
        )
      }}
    />
  )
}
