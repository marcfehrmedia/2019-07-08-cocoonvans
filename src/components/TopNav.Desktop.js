import React from 'react'
import styled from 'styled-components'
import MenuLinkComponent from './MenuLink.Desktop'

const DesktopTopNavComponent = props => {
  const mappedLinks = props.links
    .filter(el => !el.hidden)
    .map((el, i) => (
      <MenuLinkComponent key={`menu-link-wrapper-desktop-${i}`} data={el} />
    ))

  const StyledNav = styled.nav`
    display: none;
    @media screen and (min-width: ${props => props.theme.responsive.medium}) {
      display: flex;
    }
    flex-direction: row;
    align-items: space-between;
    flex-align: center;
    justify-content: flex-end;
  `

  return <StyledNav>{mappedLinks}</StyledNav>
}

export default DesktopTopNavComponent
