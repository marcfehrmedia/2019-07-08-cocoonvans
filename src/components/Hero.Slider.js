import React from 'react'
import styled from 'styled-components'
import SliderHeader from './Slider.Header'

const Wrapper = styled.section`
  position: relative;
  min-height: 300px;
`

const HeaderImageContainer = styled.div`
  width: 100vw;
  height: 60vh;
  min-height: 200px;
  max-height: 600px;
  background-size: cover;
  overflow-y: hidden;
  z-index: -5;
  position: relative;
  border-top: 0.25rem solid ${props => props.theme.colors.highlight};
  border-bottom: 0.25rem solid ${props => props.theme.colors.secondary};
`

const Title = styled.h1`
  font-size: 3em;
  text-transform: capitalize;
  font-weight: 600;
  position: absolute;
  width: 100%;
  max-width: ${props => props.theme.sizes.maxWidthCentered};
  padding: 0 1rem;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
  color: white;
`

const Hero = props => (
  <Wrapper>
    <HeaderImageContainer
      style={{
        backgroundColor: props.backgroundColor || 'transparent'
      }}
    >
      <SliderHeader sliderImages={props.sliderImages} />
    </HeaderImageContainer>
    <Title>{props.title}</Title>
  </Wrapper>
)

export default Hero
