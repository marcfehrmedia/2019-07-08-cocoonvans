import React from 'react'
import Img from 'gatsby-image'
import PageTitle from './PageTitle'
import styled from 'styled-components'

const Wrapper = styled.section`
  position: relative;
  min-height: 300px;
`

const Hero = props => (
  <Wrapper>
    <PageTitle>Galerie</PageTitle>
  </Wrapper>
)

export default Hero
