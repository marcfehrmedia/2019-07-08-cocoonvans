import React, { useEffect } from 'react'

import styled from 'styled-components'
import { Calendar, momentLocalizer } from 'react-big-calendar'
import moment from 'moment'

import myEventsList from '../constants/events'

import 'react-big-calendar/lib/sass/styles.scss'
import '../styles/animations.scss'
import 'semantic-ui-css/components/icon.min.css'

const localizer = momentLocalizer(moment)

const CalendarWrapper = styled.div`
  max-width: ${props => props.theme.sizes.maxWidthCentered};
  margin: 2rem auto;
`

const CalendarComponent = props => {
  useEffect(() => {
    console.log('Calendar component loaded')
  }, [])

  return (
    <CalendarWrapper>
      <Calendar
        localizer={localizer}
        events={myEventsList}
        startAccessor='start'
        endAccessor='end'
        style={{ height: 500 }}
      />
    </CalendarWrapper>
  )
}

export default CalendarComponent
