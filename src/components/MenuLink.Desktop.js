import React, { useState } from 'react'
import { Link } from 'gatsby'
import styled, { ThemeProvider } from 'styled-components'
import theme from '../styles/theme'

import 'semantic-ui-css/components/icon.min.css'

const MenuLinkComponent = props => {
  const [isOpen, setIsOpen] = useState(false)

  const MainLink = styled.div`
    background: transparent;
    z-index: 999;
    height: 2.5rem;
    width: auto;
    float: left;
    align-self: center;

    &.mobile-only {
      @media screen and (min-width: 1150px) {
        display: none;
      }
    }

    a {
      opacity: 0.8;
      font-family: ${props => props.theme.fonts.title};
      font-size: 1.3rem;
      @media screen and (max-width: ${props => props.theme.responsive.large}) {
        font-size: 1.3rem;
        line-height: 1.5rem;
      }
      line-height: 1.25em;
      font-weight: 800;
      color: ${props => props.theme.colors.secondary};

      &:hover {
        opacity: 1;
      }

      &.active {
        opacity: 1;
        color: ${props => props.theme.colors.highlight};
      }
    }

    width: auto;
    text-align: center;
    padding: 0.25rem 1rem 1rem 1rem;

    &.is-dropdown {
      border: none;
      opacity: 0.8;
      z-index: 999;
      position: relative;
      padding-left: 0;
      padding-right: 0;
      display: inline-block;
    }

    &:hover {
      opacity: 1;
      cursor: pointer;
    }
  `

  const SubLinkList = styled.ul`
    background: ${props => props.theme.colors.base};
    text-align: center;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    padding: 0.75rem 1rem 0.25rem 1rem;
    margin-top: -2.9rem;
    &.invisible {
      opacity: 0;
    }
    li {
      z-index: 999;
      background: ${props => props.theme.colors.base};
      position: relative;
      display: block;
      padding: 0.5rem;
      border-top: 3px solid;
      font-family: ${props => props.theme.fonts.title};
      font-weight: 400;
      border-top-color: ${props => props.theme.colors.highlight};
    }

    &.highlighted {
      background: ${props => props.theme.colors.highlight};
    }

    a {
      opacity: 0.8;
      display: block;
      position: relative;
      font-size: 1.3rem;
      color: ${props => props.theme.colors.inverted};

      @media screen and (max-width: ${props => props.theme.responsive.large}) {
        font-size: 1.3rem;
        line-height: 1.5rem;
      }

      font-family: ${props => props.theme.fonts.title};

      @media screen and (max-width: ${props => props.theme.responsive.large}) {
        font-size: 1.3rem;
        line-height: 1.5em;
      }

      &.active {
        color: ${props => props.theme.colors.highlight};
        opacity: 1;
      }
    }
  `

  const subLinksMapped = props.data.subLinks
    .filter(el => !el.hidden)
    .map((el, i) => (
      <li key={`sublink-desktop-${i}`}>
        <Link
          activeClassName='active'
          to={el.link}
          key={`desktop-menu-sublink-${i}`}
        >
          {el.icon && <i className={`icon ${el.icon}`} />}
          {el.title}
        </Link>
      </li>
    ))

  return (
    <div
      style={{
        alignItems: 'flex-end',
        display: 'inline-flex',
        overflow: isOpen ? 'visible' : 'hidden'
      }}
      onMouseLeave={() => setIsOpen(false)}
    >
      <ThemeProvider theme={theme}>
        {!props.data.isDropdown ? (
          <MainLink className={props.data.mobileOnly ? 'mobile-only' : ''}>
            <Link activeClassName='active' to={props.data.link}>
              {props.data.icon && <i className={`icon ${props.data.icon}`} />}
              {props.data.title}
            </Link>
          </MainLink>
        ) : (
          <MainLink
            className={`is-dropdown ${
              props.data.mobileOnly ? 'mobile-only' : ''
            }`}
            onMouseEnter={() => setIsOpen(true)}
          >
            <Link
              to={props.data.link}
              style={{ opacity: isOpen ? 0 : 1 }}
              activeClassName='active'
            >
              {props.data.icon && <i className={`icon ${props.data.icon}`} />}
              {props.data.title} <i className='small icon caret down' />
            </Link>
            <SubLinkList className={isOpen ? '' : 'invisible'}>
              <li
                style={{ border: 'none' }}
                key={`sublink-${props.data.title}`}
              >
                <Link
                  activeClassName='active'
                  to={props.data.link}
                  className={`${props.highlighted ? 'highlighted' : ''}`}
                >
                  {props.data.icon && (
                    <i className={`icon ${props.data.icon}`} />
                  )}
                  {props.data.title} <i className='small icon caret up' />
                </Link>
              </li>
              {subLinksMapped}
            </SubLinkList>
          </MainLink>
        )}
      </ThemeProvider>
    </div>
  )
}

export default MenuLinkComponent
