import React from 'react'
import Img from 'gatsby-image/withIEPolyfill'
import Carousel from '@brainhubeu/react-carousel'
import styled from 'styled-components'
import '../styles/slider.scss'

const StyledDiv = styled.div`
  .BrainhubCarousel {
    position: relative;
    display: grid;
    overflow-y: visible;
    padding: 2rem 0 2rem 0;
    h1,
    h2,
    h3 {
      font-size: 1.1em;
    }
    p {
      font-size: 1em;
    }
    .BrainhubCarousel__arrows {
      position: absolute;
      z-index: 9999;
      top: calc(50% - 20px);
      background: ${props => props.theme.colors.secondary};
      &:hover {
        background: ${props => props.theme.colors.highlight};
      }
      &.BrainhubCarousel__arrowLeft {
        span {
          max-width: 20px;
          max-height: 20px;
        }
      }
      &.BrainhubCarousel__arrowRight {
        right: 0;
        span {
          max-width: 20px;
          max-height: 20px;
        }
      }
    }
    .BrainhubCarousel__trackContainer {
      // position: relative;
      overflow-y: visible;
    }}
  }
`

const ImageWrapper = styled.div`
  height: 200px;
`

const CarouselSlider = props => {
  const mappedImages = props.images.map((el, i) => (
    <ImageWrapper key={`image-wrapper-${i}`}>
      <Img
        key={`preview-image-${i}`}
        fluid={el.fluid}
        objectFit="cover"
        objectPosition="50% 50%"
        style={{
          height: '200px',
          maxHeight: '600px',
        }}
        imgStyle={{
          height: '100%',
          objectFit: 'contain',
          objectPosition: '50% 50%',
        }}
      />
    </ImageWrapper>
  ))

  props.images.map(el => {
    console.log(el)
  })

  return (
    <StyledDiv>
      <Carousel
        autoPlay={4000}
        animationSpeed={250}
        infinite
        centered
        arrows
        indicators
        stopAutoPlayOnHover
        slidesPerPage={props.slidesPerPage || 3}
        slidesPerScroll={1}
        breakpoints={{
          950: {
            // these props will be applied when screen width is less than 1000px
            slidesPerPage: 3,
          },
          800: {
            // these props will be applied when screen width is less than 1000px
            slidesPerPage: 3,
          },
          650: {
            // these props will be applied when screen width is less than 1000px
            slidesPerPage: 2,
          },
          500: {
            slidesPerPage: 1,
          },
        }}
      >
        {mappedImages}
      </Carousel>
    </StyledDiv>
  )
}

export default CarouselSlider
