import React from 'react'
import styled from 'styled-components'
import StackGrid from 'react-stack-grid'
import PageTitle from './PageTitle'
import Img from 'gatsby-image'
import 'react-image-lightbox/style.css'

const InstaGrid = props => {
  const Wrapper = styled.section`
    position: relative;
    min-height: 300px;
    margin-bottom: 0;
  `

  return (
    <Wrapper>
      <PageTitle>Instagram</PageTitle>
      <StackGrid duration={0} columnWidth={props.columnWidth}>
        {props.items.map((item, i) => (
          <a
            key={`item-${i}`}
            rel='nofollow noopener noreferrer'
            target='_blank'
            href='https://www.instagram.com/cocoonvans'
          >
            <Img fluid={item.localFile.childImageSharp.fluid} />
          </a>
        ))}
      </StackGrid>
    </Wrapper>
  )
}

export default InstaGrid
