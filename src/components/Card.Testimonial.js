import React from 'react'
import styled from 'styled-components'
import Img from 'gatsby-image'
// import TestimonialBody from './TestimonialBody'

const StyledDiv = styled.div`
  text-align: left;
  padding: 0 1rem;
  .testimonial-text {
    font-family: ${props => props.theme.fonts.title};
    font-size: 2rem;
    padding: 0 0 1rem 0;
    font-weight: 800;
    line-height: 1.4;
    text-align: center;
    color: ${props => props.theme.colors.base};
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      font-size: 1em;
      line-height: 1.2;
    }
  }
  p {
    font-family: ${props => props.theme.fonts.text};
    font-weight: normal;
    text-align: center;
    color: ${props => props.theme.colors.base};
  }
  .testimonial-logo {
    text-align: left;
    border-radius: 50%;
    width: 10rem;
    height: 10rem;
    margin: 0 auto 2rem auto;
  }
`

const TestimonialCard = props => {
  return (
    <StyledDiv>
      <Img
        className='testimonial-logo'
        fluid={props.data.image.fluid}
        style={{
          objectFit: 'cover',
          objectPosition: 'center center'
        }}
        imgStyle={{ objectFit: 'cover', objectPosition: 'center center' }}
      />
      <h3 className='testimonial-text'>«{props.data.text}»</h3>
      <p>
        <span>{props.data.name}</span>
        {props.data.age && <span>, {props.data.age}</span>}
        {props.data.place && <span>, {props.data.place}</span>}
      </p>
    </StyledDiv>
  )
}

export default TestimonialCard
