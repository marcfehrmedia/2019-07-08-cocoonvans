import React from 'react'
import styled from 'styled-components'
import '../styles/gradients.scss'

const Wrapper = styled.section`
  margin: 0 auto;
  width: 100%;
  max-width: ${props => props.theme.sizes.maxWidth};
  padding: 3em 1.5em 2em;
  flex-grow: 1;

  @media screen and (max-width: ${props => props.theme.responsive.small}) {
    padding: 1rem 2rem;
  }

  &.full-height {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    min-height: calc(100vh - 100px);
    min-height: calc((var(--vh, 1vh) * 100) - 72px);
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      min-height: calc(100vh - 54px);
      min-height: calc((var(--vh, 1vh) * 100) - 54px);
    }
  }
`

const Container = props => {
  return (
    <Wrapper className={`${props.fullHeight ? 'full-height' : ''}`}>
      {props.children}
    </Wrapper>
  )
}

export default Container
