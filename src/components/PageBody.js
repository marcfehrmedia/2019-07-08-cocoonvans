import React from 'react'
import styled from 'styled-components'
import { BLOCKS, MARKS, INLINES } from '@contentful/rich-text-types'
import { documentToReactComponents } from '@contentful/rich-text-react-renderer'

const Body = styled.div`
  margin: 0 auto;
  &.left-aligned {
    margin: 0;
  }
  max-width: ${props => props.theme.sizes.maxWidthCentered};
  h1,
  h2,
  h3,
  h4 {
    font-weight: 600;
    line-height: 1.25;
    margin: 0 0 1rem 0;
  }

  h1 {
    font-size: 1.8rem;
    margin: 3rem 0 1rem 0;
    display: table;
    background: ${props => props.theme.colors.highlight};
    color: ${props => props.theme.colors.base};
    padding: 0.3rem;
    border-radius: 2px;
    @media screen and (max-width: 500px) {
      color: ${props => props.theme.colors.highlight};
      background: transparent;
      font-size: 1.25em;
      padding: 0;
    }
  }

  h2 {
    font-size: 1.5rem;
    display: table;
    margin: 2rem 0 1rem 0;
    background: ${props => props.theme.colors.tertiary};
    color: ${props => props.theme.colors.base};
    padding: 0.3rem;
    border-radius: 2px;
    @media screen and (max-width: 500px) {
      font-size: 1.5rem;
    }
  }

  h3 {
    font-size: 1.4rem;
    margin: 1rem 0 2rem 0;
  }

  h4 {
    font-size: 1.25em;
    display: block;
    color: ${props => props.theme.colors.secondary};
    font-weight: 800;
    margin: 1rem 0 2rem 0;
  }

  figcaption {
    color: #282828;
    padding: 0.5rem 1rem;
    font-family: 'Roboto', sans-serif;
    background: ${props => props.theme.colors.tertiary};
  }

  p {
    font-weight: 400;
    line-height: 1.6;
    margin: 1rem 0 1rem 0;

    a {
      border-bottom: 2px solid ${props => props.theme.colors.highlight};
      font-weight: 700;
      &:hover {
        background: ${props => props.theme.colors.highlight};
        color: ${props => props.theme.colors.base};
      }
    }
  }

  a {
    transition: 0.2s;
    color: ${props => props.theme.colors.base};

    &:hover {
      color: ${props => props.theme.colors.highlight};
    }
  }

  del {
    text-decoration: line-through;
  }
  strong {
    font-weight: 700;
  }
  em {
    font-style: italic;
  }

  ul,
  ol {
    margin: 0 0 1rem 0;
  }

  ul {
    li {
      list-style: disc;
      list-style-position: inside;
      line-height: 1.25;
      p {
        display: inline;
      }
      &:last-child {
        margin: 0;
      }
    }
  }

  ol {
    li {
      list-style: decimal;
      list-style-position: inside;
      line-height: 1.25;
      &:last-child {
        margin: 0;
      }
    }
  }

  hr {
    border-style: solid;
    border-color: ${props => props.theme.colors.tertiary};
    margin: 2rem -5rem;

    @media screen and (max-width: 500px) {
      margin: 2rem 1rem;
    }
  }

  pre {
    margin: 0 0 2em 0;
    border-radius: 2px;
    background: ${props => props.theme.colors.secondary} !important;
    span {
      background: inherit !important;
    }
  }

  blockquote {
    font-style: normal;
    border-left: 4px solid ${props => props.theme.colors.highlight};
    padding: 0 0 0 0.5em;
    font-style: none;
    p {
      font-size: 1.2rem;
      font-family: 'Oswald', sans-serif;
    }
  }

  pre {
    margin: 0 0 2em 0;
    border-radius: 2px;
    background: ${props => props.theme.colors.secondary} !important;
    span {
      background: inherit !important;
    }
  }
`

const ImgWrapper = styled.div`
  color: ${props => props.theme.colors.tertiary};
  text-align: center;
  width: 100%;
  margin: 2rem auto 2rem auto;
  overflow-y: hidden;
`

const PageBody = props => {
  const content = props.body.json

  const ImgComponent = ({ src, caption }) => (
    <figure>
      <ImgWrapper>
        <img className='rounded' src={src} style={{ width: '100%' }} />
        {caption && <figcaption>{caption}</figcaption>}
      </ImgWrapper>
    </figure>
  )

  const options = {
    renderMark: {
      [INLINES.HYPERLINK]: link => (
        <a target={link.target} rel='noopener noreferrer' href={link.href}>
          {content}
        </a>
      ),
      [MARKS.BOLD]: text => <strong>{text}</strong>,
      [BLOCKS.HEADING_1]: text => <h1>{text}</h1>,
      [BLOCKS.HEADING_2]: text => <h2>{text}</h2>,
      [BLOCKS.HEADING_3]: text => <h3>{text}</h3>,
      [BLOCKS.HEADING_4]: text => <h4>{text}</h4>,
      [BLOCKS.HEADING_5]: text => <h5>{text}</h5>,
      [BLOCKS.HEADING_6]: text => <h6>{text}</h6>,
      [BLOCKS.LIST_ITEM]: text => <li>{text}</li>,
      [BLOCKS.UL_LIST]: text => <ul>{text}</ul>,
      [BLOCKS.OL_LIST]: text => <ol>{text}</ol>
    },
    renderNode: {
      [BLOCKS.PARAGRAPH]: (node, children) => <p>{children}&nbsp;</p>,
      [BLOCKS.EMBEDDED_ASSET]: node => {
        // console.log(node)
        const { file, description } = node.data.target.fields
        // console.log(file["en-US"].url)
        if (description) {
          return (
            <ImgComponent
              src={file['de-CH'].url}
              caption={description['de-CH'] || null}
            />
          )
        } else {
          return <ImgComponent src={file['de-CH'].url} />
        }
      }
    },
    renderText: text =>
      text.split('\n').flatMap((text, i) => [i > 0 && <br />, text])
  }

  return (
    <Body className={props.className}>
      {documentToReactComponents(props.body.json, options)}
    </Body>
  )
}

export default PageBody
