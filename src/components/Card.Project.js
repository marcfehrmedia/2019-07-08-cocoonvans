import React from 'react'
import styled from 'styled-components'
import { Link } from 'gatsby'
import TagList from '../components/TagList'
import Img from 'gatsby-image'

const Post = styled.li`
  position: relative;
  border-bottom: 0px solid ${props => props.theme.colors.primary};
  border-radius: 0;
  margin: 0 0 1em 0;
  width: 100%;
  transition: background 0.2s;
  border-bottom: 1px solid ${props => props.theme.colors.highlight};
  box-shadow: 0px 0px 17px -2px rgba(0, 0, 0, 0.3);
  -webkit-box-shadow: 0px 0px 17px -2px rgba(0, 0, 0, 0.3);
  -moz-box-shadow: 0px 0px 17px -2px rgba(0, 0, 0, 0.3);

  @media screen and (min-width: ${props => props.theme.responsive.small}) {
    border-bottom: none;
    flex: ${props => (props.featured ? '0 0 100%' : '0 0 49%')};
    margin: 0 0 2vw 0;
  }
  @media screen and (min-width: ${props => props.theme.responsive.medium}) {
    flex: ${props => (props.featured ? '0 0 100%' : '0 0 32%')}; 
  }

  &:hover {
    box-shadow: 0px 0px 17px -2px rgba(0, 0, 0, 0.7);
    -webkit-box-shadow: 0px 0px 17px -2px rgba(0, 0, 0, 0.7);
    -moz-box-shadow: 0px 0px 17px -2px rgba(0, 0, 0, 0.7);
  }
  a {
    display: flex;
    flex-flow: column;
    height: 100%;
    width: 100%;
    color: ${props => props.theme.colors.base};
    text-decoration: none;
    .gatsby-image-wrapper {
      height: 0;
      padding-bottom: 60%;
      @media screen and (min-width: ${props => props.theme.responsive.small}) {
        padding-bottom: ${props => (props.featured ? '40%' : '60%')};
      }
    }
  }
`

const Title = styled.h2`
  color: ${props => props.theme.colors.primary};
  font-size: 1.5em;
  font-weight: 600;
  text-transform: uppercase;
  margin: 1rem 1rem 0.5rem 1rem;
`

const VehicleText = styled.h3`
  margin: 1rem 1rem 0.5rem 1rem;
  color: gray;
`

const DoneBy = styled.h4`
  margin: 0 1rem 1.5rem 1rem;
  color: gray;
`

const Excerpt = styled.p`
  margin: 0 1rem 1rem 1rem;
  line-height: 1.6;
`

const CardProject = ({
  slug,
  heroImage,
  title,
  publishVehicleText,
  dauer,
  fertigstellung,
  fahrzeug,
  tags,
  text,
  ...props
}) => {
  return (
    <Post featured={props.featured}>
      <Link to={`/ausbau/projekte/${slug}/`}>
        <Img fluid={heroImage.fluid} backgroundColor={'#eeeeee'} />
        <Title>{title}</Title>
        <VehicleText>🚐 {fahrzeug}</VehicleText>
        {tags && <TagList inline tags={tags} />}
      </Link>
    </Post>
  )
}

export default CardProject
