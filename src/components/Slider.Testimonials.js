import React, { useState, useEffect } from 'react'
import Carousel from '@brainhubeu/react-carousel'
import styled from 'styled-components'
import TestimonialCard from './Card.Testimonial'
import '../styles/slider.scss'

const StyledDiv = styled.div`
  p {
    line-height: 1.25;
  }
  .testimonial-text {
    color: ${props => props.theme.colors.highlight};
    max-width: 600px;
    @media screen and (max-width: ${props => props.theme.responsive.small}) {
      font-size: 1em;
    }
  }
  .BrainhubCarouselItem {
    padding: 2rem 0;
    align-items: flex-start;
  }
  .BrainhubCarousel__arrows {
    background: ${props => props.theme.colors.highlight};
    opacity: 0.8;
    &:hover {
      opacity: 1;
      background: ${props => props.theme.colors.highlight} !important;
    }
  }
`

const SliderTestimonials = props => {
  const [shuffledArray, setShuffledArray] = useState(props.data)

  const shuffle = a => {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1))
      ;[a[i], a[j]] = [a[j], a[i]]
    }
    return a
  }

  useEffect(() => {
    setShuffledArray(shuffle(props.data))
  })

  const mappedTestimonials = shuffle(shuffledArray).map((el, i) => (
    <TestimonialCard key={`testimonial-${i}`} data={el} />
  ))

  return (
    <StyledDiv>
      <Carousel
        infinite
        draggable={props.data.length > 1}
        autoPlay={props.data.length > 1 ? 5000 : null}
        stopAutoPlayOnHover
        arrows={props.data.length > 1}
        slidesPerPage={1}
        slidesPerScroll={1}
        breakpoints={{
          950: {
            // these props will be applied when screen width is less than 1000px
            slidesPerPage: props.data.length > 1 ? 2 : 1,
            slidesPerScroll: 1,
          },
          500: {
            slidesPerPage: 1,
            slidesPerScroll: 1,
            itemWidth: '100%',
          },
        }}
      >
        { mappedTestimonials }
      </Carousel>
    </StyledDiv>
  )
}

export default SliderTestimonials
