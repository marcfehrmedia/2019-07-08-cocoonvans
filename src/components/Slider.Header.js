import React from 'react'
import Img from 'gatsby-image/withIEPolyfill'
import styled from 'styled-components'

const fadeProperties = {
  duration: 4000,
  arrows: true,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  pauseOnHover: true,
  prevArrow: (
    <div
      style={{
        width: '30px',
        position: 'absolute',
        top: 'calc(50% - 30px)',
        margin: 0,
        left: '.5rem',
      }}
    >
      <i className="icon big yellow long arrow alternate left" />
    </div>
  ),
  nextArrow: (
    <div
      style={{
        right: '2rem',
        width: '30px',
        top: 'calc(50% - 30px)',
        margin: 0,
        left: 'unset',
        position: 'absolute',
      }}
    >
      <i className="icon big yellow long arrow alternate right" />
    </div>
  ),
}

const StyledDiv = styled.div`
  position: relative;
`

const Hero = props => {
  const mappedHeaders = props.sliderImages.map((el, i) => (
    <ImgWrapper key={`slider-header-${i}`}>
      <Img
        fluid={el.fluid}
        key={`header-image-${i}`}
        objectFit="cover"
        objectPosition="50% 50%"
        style={{
          height: '60vh',
          maxHeight: '600px',
        }}
        imgStyle={{
          height: '100%',
          objectFit: 'cover',
          objectPosition: '50% 50%',
        }}
      />
    </ImgWrapper>
  ))

  return (
    <StyledDiv className="slide-container">
      <Slider {...settings}>
        {mappedHeaders}
      </Slider>
    </StyledDiv>
  )
}

export default Hero
