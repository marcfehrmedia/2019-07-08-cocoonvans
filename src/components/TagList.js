import React from 'react'
import styled from 'styled-components'
import { Link } from 'gatsby'
import 'semantic-ui-css/components/segment.min.css'
import 'semantic-ui-css/components/label.min.css'

const List = styled.ul`
  width: 100%;
 max-width: ${props => props.theme.sizes.maxWidthCentered};
  &:not(.inline) {
    margin: 0 auto 1em auto;
  }
`

const TagButton = styled.button `
  margin-bottom: 5px !important;
  font-family: 'Roboto', sans-serif !important;
`

const Tag = styled.button`
  display: ${props => (props.inline ? 'inline' : 'inline-block')};
  margin: 0 0.25em 0.25em 0;
  font-family: 'Roboto', sans-serif !important;
  
  a {
    float: left;
    transition: 0.2s;
    background: ${props => props.theme.colors.tertiary};
    padding: 0.5em;
    border-radius: 2px;
    text-transform: capitalize;
    text-decoration: none;
    font-family: 'Roboto', sans-serif !important;
    color: ${props => props.theme.colors.base};
    border: 1px solid ${props => props.theme.colors.secondary};
    &:hover {
      background: ${props => props.theme.colors.secondary};
    }
  }
`

const LabelContainer = styled.div`
  padding: .5rem;
  margin-bottom: 0;
`

const Label = styled.button`
  margin: 0.5rem 0.5rem 0rem 0 !important;
  font-family: 'Roboto', sans-serif;
  border-radius: 0 !important;
  color: rgba(0,0,0,0.8) !important;
  font-family: 'Roboto', sans-serif !important;
  
  * {
    color: rgba(0,0,0,0.8) !important;
  }
`

const TagList = props => {
  return (
    <List className={props.inline ? 'inline' : ''}>
      {!props.inline && (
        <Link to="/ausbau/projekte">
          <Label className="ui tiny basic icon button" style={{marginRight: '1rem', fontWeight: 'bold'}}>
            <i className="ui icon long arrow left" />
            Alle Projekte
          </Label>
        </Link>
      )}
      {props.inline ? (
        <LabelContainer>
          {props.tags.map((tag, i) => (
            <Label className="ui tiny yellow button" key={tag.id}>
              <Link key={`tag-${i}`} to={`/ausbau/tag/${tag.slug}/`}>
                {tag.title}
              </Link>
            </Label>
          ))}
        </LabelContainer>
      ) : (
        props.tags.map((tag, i) => (
          <Link key={`tag-${i}`} to={`/ausbau/tag/${tag.slug}/`}>
            <Label className="ui tiny yellow button" key={tag.id}>
              {tag.title}
            </Label>
          </Link>
        ))
      )}
    </List>
  )
}

export default TagList
