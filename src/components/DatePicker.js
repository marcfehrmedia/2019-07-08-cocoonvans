import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { DateRangePicker } from 'react-dates'
import styled from 'styled-components'
import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'
import 'semantic-ui-css/components/icon.css'
import 'semantic-ui-css/components/message.css'

import Moment from 'moment'
import { extendMoment } from 'moment-range'
import deLocale from 'moment/locale/de'
import RentalForm from './RentalForm'

const moment = extendMoment(Moment)

const CalendarWrapper = styled.div`
  max-width: ${props => props.theme.sizes.maxWidthCentered};
  margin: 2rem auto;
  text-align: center;
  z-index: 9999;

  .DateInput_input__focused {
    border-color: ${props => props.theme.colors.highlight} !important;
  }

  .CalendarDay__selected,
  .CalendarDay__selected:active,
  .CalendarDay__selected:hover {
    background: ${props => props.theme.colors.highlight} !important;
    border-color: ${props => props.theme.colors.highlight} !important;
  }
`

function DatepickerComponent(props) {
  /* Handle Airbnb datepicker */
  const initialState = { startDate: null, endDate: null }
  const [dateRange, setdateRange] = useState(initialState)
  const [focus, setFocus] = useState(null)
  const [availability, setAvailability] = useState(null)

  const { startDate, endDate } = dateRange

  /* Fetch Google Calendar entries */
  const [events, setEvents] = useState([])
  const [calendarIsReady, setCalendarIsReady] = useState(false)

  const setCalendarEvents = e => {
    const allEvents = e.items
    // console.log(e.items.length + ' events found')
    // console.log(allEvents)
    const blockedDaysArray = []
    // eslint-disable-next-line no-plusplus
    for (let i = 0; i < allEvents.length; i++) {
      const start =
        moment(allEvents[i].start.date) || moment(allEvents[i].start.dateTime)
      const end =
        moment(allEvents[i].end.date) || moment(allEvents[i].end.dateTime)
      const r1 = moment.range(start, end)
      const dateArray = Array.from(r1.by('days')).map(m => m)
      // eslint-disable-next-line no-plusplus
      for (let ii = 0; ii < dateArray.length - 1; ii++) {
        if (dateArray[ii].isAfter(moment().subtract(1, 'days'))) {
          blockedDaysArray.push(dateArray[ii])
        }
      }
    }
    // console.log(blockedDaysArray)
    setCalendarIsReady(true)
    setEvents(blockedDaysArray)
  }

  useEffect(() => {
    fetch(
      `https://www.googleapis.com/calendar/v3/calendars/${props.calendarID}/events?key=AIzaSyBjZF8cdH1cTPvfyWEbvRnPj5I6ArnMMHQ`
    )
      .then(response => response.json())
      .then(data => setCalendarEvents(data))
  }, [])

  const StyledDiv = styled.div`
    margin: 2rem 0;
  `

  const isBrowser = typeof window !== `undefined`

  const handleOnDateChange = (startDate, endDate) => {
    setdateRange(startDate, endDate)
  }

  const blockedDates = events || []

  const isDayBlocked = day => {
    // Loop through blocked days from Google Cal and check if in array
    return blockedDates.some(unavailableDay =>
      moment(unavailableDay).isSame(day, 'day')
    )
  }

  const rangeHasBlockedDates = (startDate, endDate) => {
    let hasError = false

    const diff = moment(startDate.endDate).diff(startDate.startDate, 'days') + 1
    console.log(diff)

    for (let i = 0; i < diff; i++) {
      console.log(i)
      const checkDate = moment(startDate.startDate).add(i, 'd')
      if (
        blockedDates.some(unavailableDay =>
          checkDate.isSame(unavailableDay, 'day')
        )
      ) {
        hasError = true
      }
    }

    if (!hasError) {
      setAvailability(true)
      handleOnDateChange(startDate, endDate)
      props.handleDate(startDate, endDate)
    } else {
      setAvailability(false)
      setdateRange(initialState)
      console.log('Blocked day in range found!')
    }
  }

  moment.locale('de', deLocale)

  return (
    <CalendarWrapper>
      {calendarIsReady && (
        <DateRangePicker
          startDatePlaceholderText='Abholung'
          startDate={startDate}
          endDatePlaceholderText='Rückgabe'
          onDatesChange={rangeHasBlockedDates}
          endDate={endDate}
          numberOfMonths={1}
          displayFormat='DD.MM.YYYY'
          showClearDates
          focusedInput={focus}
          onFocusChange={focus => setFocus(focus)}
          startDateId='startDateRental'
          endDateId='endDateRental'
          minimumNights={3}
          isDayBlocked={day => isDayBlocked(day)}
          hideKeyboardShortcutsPanel
        />
      )}
      <StyledDiv>
        {availability !== null && startDate !== null && endDate !== null && (
          <p style={{ display: 'none' }}>
            <i
              className={`${
                availability && startDate !== null && endDate !== null
                  ? 'green'
                  : 'red'
              } icon circle`}
            />
            {!availability ? 'Nicht verfügbar' : 'Verfügbar'}
          </p>
        )}

        {/* TODO: Build in some useful error messages */}

        {!startDate && !endDate && !availability && (
          <div className='ui icon alert message' style={{ borderRadius: 0 }}>
            <i className='icon calendar alternate' />
            Bitte wähle mindestens vier aufeinanderfolgende Tage aus, ohne dass
            geblockte Tage in deinem gewünschten Zeitfenster enthalten sind.
          </div>
        )}

        {startDate &&
          endDate &&
          availability &&
          moment(endDate).diff(startDate, 'days') + 1 < 4 && (
            <div className='ui icon error message' style={{ borderRadius: 0 }}>
              <i className='icon remove' />
              Bitte wähle mindestens vier aufeinanderfolgende Tage aus
              (Mindestmietdauer).
            </div>
          )}

        <div
          style={{
            display:
              (startDate &&
                endDate &&
                availability &&
                moment(endDate).diff(startDate, 'days') + 1 >= 4 &&
                availability) ||
              !isBrowser
                ? 'block'
                : 'none'
          }}
        >
          <p style={{ display: 'none' }}>
            Von:
            {startDate ? startDate.format('L') : ''}
          </p>
          <p style={{ display: 'none' }}>
            Bis:
            {endDate ? endDate.format('L') : ''}
          </p>
          <p style={{ display: 'none' }}>
            Anzahl Tage:{' '}
            {endDate && startDate
              ? moment(endDate).diff(startDate, 'days') + 1
              : ''}
          </p>

          <RentalForm
            startDate={startDate || moment()}
            endDate={endDate || moment()}
            duration={moment(endDate).diff(startDate, 'days') + 1 || 1}
            vanName={props.vanName || 'undefined'}
          />
        </div>
      </StyledDiv>
    </CalendarWrapper>
  )
}

export default DatepickerComponent
