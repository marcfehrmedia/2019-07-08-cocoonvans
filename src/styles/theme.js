const theme = {
  colors: {
    base: 'rgba(0,0,0,0.9)', // Black
    secondary: '#bbb', // Medium Gray
    tertiary: '#eee', // Light Gray
    highlight: '#FEC340', // Light Blue
    inverted: '#FFFFFF'
  },
  gradients: {
    punyeta: {
      backgroundOne: '#108dc7',
      backgroundTwo: '-webkit-linear-gradient(to bottom, #ef8e38, #108dc7)',
      backgroundThree: 'linear-gradient(to bottom, #ef8e38, #108dc7);'
    }
  },
  sizes: {
    maxWidth: '1050px',
    maxWidthCentered: '650px'
  },
  responsive: {
    small: '35em',
    smallMedium: '45em',
    medium: '50em',
    mediumLarge: '60em',
    large: '70em'
  },
  fonts: {
    title: '"Oswald", sans-serif',
    text: '"Roboto", sans-serif'
  }
}

export default theme
