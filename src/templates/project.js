import React from 'react'
import { graphql, Link } from 'gatsby'
import Helmet from 'react-helmet'
import config from '../utils/siteConfig'
import Layout from '../components/Layout'
import HeroImage from '../components/Hero.Image'
import Container from '../components/Container'
import PageTitle from '../components/PageTitle'
import PageBody from '../components/PageBody'
import PostGalleryGrid from '../components/PostGrid'
import TagList from '../components/TagList'
import PostDetails from '../components/PostDetails'
import SEO from '../components/SEO'

const ProjectTemplate = ({ data, pageContext }) => {
  const {
    title,
    slug,
    gallery,
    dauer,
    kunde,
    fahrzeug,
    heroImage,
    text,
    fertigstellung,
    tags
  } = data.contentfulPost
  const postNode = data.contentfulPost

  const previous = pageContext.prev
  const next = pageContext.next

  return (
    <Layout>
      <Helmet>
        <title>{`${title} - ${config.siteTitle}`}</title>
      </Helmet>
      <SEO pagePath={slug} postNode={postNode} postSEO />

      <HeroImage title={``} image={heroImage} height={'50vh'} />

      <Container>
        <PageTitle>{title}</PageTitle>
        {tags && <TagList tags={tags} />}

        <PostDetails
          dauer={dauer}
          vehicle={fahrzeug}
          doneBy={fertigstellung}
          customer={kunde}
        />
        <PageBody body={text} />
      </Container>
      <PostGalleryGrid items={gallery} columnWidth='33.33%' title='Galerie' />
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    contentfulPost(slug: { eq: $slug }) {
      title
      slug
      metaDescription {
        internal {
          content
        }
      }
      tags {
        title
        id
        slug
      }
      heroImage {
        title
        fluid(maxWidth: 1800) {
          ...GatsbyContentfulFluid_withWebp_noBase64
        }
        ogimg: resize(width: 1800) {
          src
          width
          height
        }
      }
      text {
        json
      }
      fahrzeug
      dauer
      kunde
      fertigstellung
      gallery {
        id
        contentful_id
        title
        fluid {
          srcSet
          src
          base64
          srcWebp
          srcSetWebp
        }
      }
    }
  }
`

export default ProjectTemplate
