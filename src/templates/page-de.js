import React from 'react'
import { graphql } from 'gatsby'
import Helmet from 'react-helmet'
import config from '../utils/siteConfig'
import Layout from '../components/Layout'
import Container from '../components/Container'
import PageTitle from '../components/PageTitle'
import PageBody from '../components/PageBody'
import SEO from '../components/SEO'

// eslint-disable-next-line react/prop-types
const PageTemplate = ({ data }) => {
  // eslint-disable-next-line react/prop-types
  const { title, slug, body, text } = data.contentfulPage
  // eslint-disable-next-line react/prop-types
  const postNode = data.contentfulPage

  return (
    // eslint-disable-next-line react/jsx-filename-extension
    <Layout>
      <Helmet>
        <title>{`${title} - ${config.siteTitle}`}</title>
      </Helmet>
      <SEO pagePath={slug} postNode={postNode} pageSEO />

      <Container>
        <PageTitle>{title}</PageTitle>
        {text && <PageBody body={text} />}
      </Container>
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    contentfulPage(node_locale: { eq: "de-CH" }, slug: { eq: $slug }) {
      title
      slug
      metaDescription {
        internal {
          content
        }
      }
      text {
        json
      }
    }
  }
`

export default PageTemplate
