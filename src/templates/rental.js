import React, { useState } from 'react'
import { graphql, Link } from 'gatsby'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import config from '../utils/siteConfig'
import Layout from '../components/Layout'
// import Hero from '../components/Hero.Slider'
import HeroImage from '../components/Hero.Image'
import Container from '../components/Container'
import PageTitle from '../components/PageTitle'
import PostGalleryGrid from '../components/PostGrid'
import PageBody from '../components/PageBody'
import SEO from '../components/SEO'
// import Collapsible from 'react-collapsible'
import loadable from '@loadable/component'
import moment from 'moment'
import RentalForm from '../components/RentalForm'
import SliderTestimonials from '../components/Slider.Testimonials'

const DatePickerComponent = loadable(() => import('../components/DatePicker'))

const RentalTemplate = ({ data, pageContext }) => {
  const {
    title,
    slug,
    gallery,
    additionalInfo,
    // features,
    description,
    text,
    calendarID,
    testimonials
  } = data.contentfulRentalVan
  const rentalNode = data.contentfulRentalVan

  const isBrowser = typeof window !== `undefined`

  const [date, setDate] = useState(null)
  const [currentCalendarID, setCurrentCalendarID] = useState(calendarID)

  const postNode = {
    title: `Vermietung: ${title}`
  }

  const TestimonialContainer = styled.div`
    max-width: ${props => props.theme.sizes.maxWidthCentered};
    margin: 0 auto 3rem auto;

    h1 {
      margin-bottom: 1rem;
    }
  `

  const TextContainer = styled.div`
    max-width: ${props => props.theme.sizes.maxWidthCentered};
    margin: 0 auto;

    &.centered {
      text-align: center;
    }
  `

  const SpacerElement = styled.div``

  return (
    <Layout>
      <Helmet>
        <title>{`${title} - ${config.siteTitle}`}</title>
      </Helmet>
      <SEO pagePath={`/vermietung/${slug}`} />

      <HeroImage title={``} image={gallery[0]} height={'50vh'} />

      <Container>
        <PageTitle>
          {title}, {description}
        </PageTitle>
        <TextContainer className={'centered'}>
          <p>Wähle deine gewünschte Mietdauer:</p>
        </TextContainer>
        <DatePickerComponent
          handleDate={(start, end) => setDate(start, end)}
          date={date}
          calendarID={currentCalendarID}
          vanName={title}
        />
        {text && <PageBody body={text} />}
        <SpacerElement />
        {/* { additionalInfo &&
        <Collapsible
          trigger={<PageTitle link style={{cursor: 'pointer'}}>Weitere Informationen <i className={'icon chevron right'} /></PageTitle>}
          triggerWhenOpen={<PageTitle link>Weniger Informationen <i className={'icon chevron up'} /></PageTitle>}
        >
          <PageBody body={additionalInfo} />
        </Collapsible>
        } */}
        <br />
        <br />
        <br />
      </Container>
      {testimonials && (
        <TestimonialContainer>
          <PageTitle>... About {title}</PageTitle>
          <SliderTestimonials data={testimonials} />
        </TestimonialContainer>
      )}
      <div style={{ display: 'none' }}>
        <RentalForm
          startDate={moment()}
          endDate={moment()}
          duration={1}
          vanName={title || 'undefined van name'}
        />
      </div>
      <PostGalleryGrid
        items={gallery}
        key={`gallery-${title}`}
        columnWidth='33.33%'
        title={`${title}'s Galerie`}
      />
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    contentfulRentalVan(slug: { eq: $slug }) {
      id
      slug
      title
      text {
        json
      }
      excerpt {
        json
      }
      # additionalInfo {
      #   json
      # }
      calendarID
      description
      testimonials {
        id
        name
        place
        text
        age
        date(formatString: "DD. MMMM YYYY", locale: "de-CH")
        image {
          fluid {
            srcWebp
            base64
            srcSetWebp
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
      gallery {
        id
        contentful_id
        title
        file {
          url
        }
        fluid {
          srcWebp
          srcSet
          src
          base64
          srcSetWebp
          ...GatsbyContentfulFluid_withWebp
        }
      }
    }
  }
`

export default RentalTemplate
