import React from 'react'
import { graphql, Link } from 'gatsby'
import orderBy from 'lodash/orderBy'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import moment from 'moment'
import config from '../utils/siteConfig'
import Layout from '../components/Layout'
import CardProject from '../components/Card.Project'
import CardList from '../components/CardList'
import PageTitle from '../components/PageTitle'
import Pagination from '../components/Pagination'
import Container from '../components/Container'
import 'semantic-ui-css/components/segment.css'

const TagTemplate = ({ data, pageContext }) => {
  const posts = orderBy(
    data.contentfulTag.post,
    // eslint-disable-next-line
    [object => new moment(object.publishDateISO)],
    ['desc']
  )

  const Label = styled.button`
    margin-bottom: 4px !important;
    font-family: 'Roboto', sans-serif;
    border-radius: 0 !important;
    color: rgba(0, 0, 0, 0.8) !important;
    font-family: 'Roboto', sans-serif !important;

    * {
      color: rgba(0, 0, 0, 0.8) !important;
    }
  `

  const { title, slug } = data.contentfulTag
  const numberOfPosts = posts.length
  const skip = pageContext.skip
  const limit = pageContext.limit
  const currentPage = pageContext.currentPage
  const isFirstPage = currentPage === 1

  return (
    <Layout>
      {isFirstPage ? (
        <Helmet>
          <title>{`Tag: ${title} - ${config.siteTitle}`}</title>
          <meta
            property="og:title"
            content={`Tag: ${title} - ${config.siteTitle}`}
          />
          <meta property="og:url" content={`${config.siteUrl}/tag/${slug}/`} />
        </Helmet>
      ) : (
        <Helmet>
          <title>{`Tag: ${title} - Page ${currentPage} - ${config.siteTitle}`}</title>
          <meta
            property="og:title"
            content={`Tag: ${title} - Page ${currentPage} - ${config.siteTitle}`}
          />
          <meta property="og:url" content={`${config.siteUrl}/tag/${slug}/`} />
        </Helmet>
      )}

      <Container>
        <PageTitle small>
          {numberOfPosts} {numberOfPosts > 1 ? 'Projekte' : 'Projekt'} mit dem
          Tag &ldquo;
          {title}
          &rdquo; gefunden:
        </PageTitle>

        <div className={'ui vertical aligned very basic segment'}>
          <Link to={'/ausbau/projekte'}>
            <Label
              className={'ui tiny basic icon button'}
              style={{
                fontfamily: 'Roboto, sans-serif !important',
                marginRight: '1rem',
                fontWeight: 'bold',
              }}
            >
              <i className={'ui icon long arrow left'} />
              Alle Projekte
            </Label>
          </Link>
        </div>

        <div style={{ minHeight: '70vh' }}>
          <CardList>
            {posts.slice(skip, limit * currentPage).map(post => (
              <CardProject {...post} key={post.id} />
            ))}
          </CardList>
        </div>
      </Container>
      <Pagination context={pageContext} />
    </Layout>
  )
}

export const query = graphql`
  query($slug: String!) {
    contentfulTag(slug: { eq: $slug }) {
      title
      id
      slug
      post {
        id
        title
        dauer
        fahrzeug
        kunde
        fertigstellung
        slug
        tags {
          slug
          title
        }
        publishDate(formatString: "MMMM DD, YYYY")
        publishDateISO: publishDate(formatString: "YYYY-MM-DD")
        heroImage {
          title
          fluid(maxWidth: 1800) {
            ...GatsbyContentfulFluid_withWebp_noBase64
          }
        }
      }
    }
  }
`

export default TagTemplate
