import React from 'react'
import { graphql } from 'gatsby'
import Helmet from 'react-helmet'
import Layout from '../components/Layout'
import CardList from '../components/CardList'
import CardProject from '../components/Card.Project'
import Container from '../components/Container'
import Pagination from '../components/Pagination'
import SEO from '../components/SEO'
import config from '../utils/siteConfig'
import PageTitle from '../components/PageTitle'
import PageBody from '../components/PageBody'

const Projects = ({ data, pageContext }) => {
  const projects = data.allContentfulPost.edges
  const page = data.contentfulPage
  const { currentPage } = pageContext
  const isFirstPage = currentPage === 1

  return (
    <Layout>
      <SEO />
      {!isFirstPage && (
        <Helmet>
          <title>{`Projekte – ${config.siteTitle}`}</title>
        </Helmet>
      )}
      <Container>
        <PageTitle>{page.title}</PageTitle>
        {isFirstPage ? (
          <div>
            <PageBody body={page.text} />
            <br />
            <br />
            <br />
            <CardList>
              {projects.map(({ node: post }) => (
                <CardProject key={post.id} {...post} />
              ))}
            </CardList>
          </div>
        ) : (
          <div>
            <CardList>
              {projects.map(({ node: post }) => (
                <CardProject key={post.id} {...post} />
              ))}
            </CardList>
          </div>
        )}
      </Container>
      <Pagination context={pageContext} />
    </Layout>
  )
}

export const query = graphql`
  query($skip: Int!, $limit: Int!) {
    contentfulPage(slug: { eq: "projekte" }) {
      id
      text {
        json
      }
      title
      text {
        json
      }
    }
    allContentfulPost(
      sort: { fields: [publishDate], order: DESC }
      limit: $limit
      skip: $skip
      filter: { node_locale: { eq: "de-CH" } }
    ) {
      edges {
        node {
          title
          id
          slug
          fertigstellung
          dauer
          text {
            json
          }
          kunde
          tags {
            title
            slug
          }
          fahrzeug
          publishDate(formatString: "MMMM DD, YYYY")
          heroImage {
            title
            fluid(maxWidth: 1800) {
              ...GatsbyContentfulFluid_withWebp_noBase64
            }
          }
        }
      }
    }
  }
`
export default Projects
