module.exports = {
  siteTitle: 'Cocoon Vans',
  siteTitleAlt: 'Cocoonvans', // This allows an alternative site title for SEO schema.
  publisher: 'Marc Fehr Media', // Organization name used for SEO schema
  siteDescription:
    'Individuelle Camper-Ausbauten 🇨🇭100% Handmade in Switzerland 📋️ Umfassende Projekt-Beratung',
  siteUrl: 'https://www.cocoonvans.com', // Site domain. Do not include a trailing slash! If you wish to use a path prefix you can read more about that here: https://www.gatsbyjs.org/docs/path-prefix/
  projectsPerHomePage: 7, // Number of posts shown on the 1st page of of the index.js template (home page)
  projectsPerPage: 6, // Number of posts shown on paginated pages
  author: 'Cocoon Vans', // Author for RSS author segment and SEO schema
  authorUrl: 'https://www.marcfehr.ch', // URL used for author and publisher schema, can be a social profile or other personal site
  userTwitter: '@cocoonvans', // Change for Twitter Cards
  shortTitle: 'Cocoonvans', // Used for App manifest e.g. Mobile Home Screen
  shareImage: '/logos/share.jpg', // Open Graph Default Share Image. 1200x1200 is recommended
  shareImageWidth: 900, // Change to the width of your default share image
  shareImageHeight: 600, // Change to the height of your default share image
  siteLogo: '/logos/logo-1024.png', // Logo used for SEO, RSS, and App manifest
  backgroundColor: '#e9e9e9', // Used for Offline Manifest
  themeColor: '#121212', // Used for Offline Manifest
  copyright: 'Copyright © 2020 Cocoon Vans', // Copyright string for the RSS feed
  menuLinks: [
    {
      link: '/',
      icon: 'home',
      title: 'Home',
      isDropdown: false,
      subLinks: [],
      hidden: true,
    },
    {
      link: '/ausbau/angebot',
      icon: 'heart outline',
      title: 'Ausbau',
      isDropdown: true,
      subLinks: [
        {
          icon: 'list',
          link: '/ausbau/angebot',
          title: 'Angebot',
        },
        {
          icon: 'tasks',
          link: '/ausbau/prozess',
          title: 'Prozess',
        },
        {
          icon: 'images outline',
          link: '/ausbau/projekte',
          title: 'Projekte',
        },
      ],
    },
    {
      link: 'https://shop.cocoonvans.com',
      title: 'Shop',
      icon: 'shopping cart',
      isDropdown: false,
      subLinks: [],
    },
    {
      link: '/vermietung',
      title: 'Vermietung',
      icon: 'calendar alternate outline',
      isDropdown: true,
      subLinks: [
        {
          icon: 'bus',
          link: '/vermietung',
          title: 'Unsere Vans',
        },
        {
          icon: 'clipboard outline',
          link: '/vermietung/kodex',
          title: 'Mietkodex',
          hidden: true,
        },
        {
          icon: 'download',
          link: '/vermietung/downloads',
          title: 'Downloads',
        },
        {
          icon: 'calendar alternate outline',
          link: '/vermietung/vans/jay',
          highlighted: true,
          title: 'Jetzt buchen',
        },
      ],
    },
    {
      link: '/about',
      icon: 'hand peace outline',
      title: 'Über uns',
      isDropdown: false,
      subLinks: [],
    },
    {
      icon: 'envelope outline',
      link: '/contact',
      title: 'Kontakt',
      isDropdown: false,
      subLinks: [],
    },
  ]

}
