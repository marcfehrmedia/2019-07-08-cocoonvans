import React from 'react'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import { graphql } from 'gatsby'
import config from '../utils/siteConfig'
import Layout from '../components/Layout'
import Container from '../components/Container'
import PageTitle from '../components/PageTitle'
import RibbonComponent from '../components/Ribbon'
import SocialButton from '../components/SocialButton'
import AdBlock from '../components/AdBlock'
import '../styles/animations.scss'
import SEO from '../components/SEO'
import 'semantic-ui-css/components/flag.css'
import 'semantic-ui-css/components/icon.css'

const Contact = ({ data }) => {
  const postNode = {
    title: `Willkommen bei ${config.siteTitle}`,
  }

  const SocialIcons = styled.div`
    padding-top: 1rem;
    @media screen and (max-width: 550px) {
      padding-top: 0.8rem;
    }
    width: 80%;
    text-align: center;
    .ui.basic.buttons .button,
    .ui.basic.button {
      width: auto;
      @media screen and (max-width: 550px) {
        width: 48%;
        float: left;
        display: block;
        margin-bottom: 0.35rem;
        font-size: 85%;
      }
    }
  `

  const AdContainer = styled.div`
    margin-top: 3rem;
    
    @media screen and (max-width: 500px) {
      margin-top: 6.5rem;
    }
  `

  const MainContent = styled.div`
    width: 100%;
    text-align: center;
    flex: auto;
    margin: 0;
  `

  const PageDescription = styled.ul`
    margin: 0 auto;
    text-align: center;
    display: flex;
    justify-content: center;

    @media screen and (max-width: 500px) {
      flex-direction: column;
    }

    li {
      list-style: none;
      padding: 0 10px;
      font-weight: 400;
      &:not(:last-child) {
        border-right: 1px solid rgba(0, 0, 0, 0.8);
        @media screen and (max-width: 850px) {
          border: none;
        }
      }
      @media screen and (max-width: 850px) {
        border: none;
        float: none;
        text-align: center;
        width: 100%;
        padding-bottom: 0.5rem;
      }
    }
  `

  const Logo = styled.img`
    margin: 3rem auto 2rem auto;  

    @media screen and (max-width: 500px) {
      margin: 2rem auto;
    }
  `

  return (
    <Layout>
      <Helmet>
        <title>{`${config.siteTitle}`}</title>
      </Helmet>
      <SEO postNode={postNode} pagePath="landing" customTitle />

      <RibbonComponent
        position="bottom-left"
        color="yellow"
        text="Shop"
        icon="shopping cart"
        link="https://shop.cocoonvans.com"
      />

      <Container fullHeight>
        <MainContent>
          <Logo
            className="logo"
            style={{
              width: '45%',
              marginBottom: '1rem',
            }}
            src={require('../assets/1x/logo.png')}
          />
          <PageTitle noMargin>Cocoon Vans</PageTitle>
          <PageDescription style={{ paddingTop: '1rem' }}>
            <li>🚐 Individuelle Camper-Ausbauten</li>
            <li>🇨🇭 100% Handmade in Switzerland</li>
            <li> 🙌🏼 Einzigartige Van-Vermietung</li>
          </PageDescription>
          <SocialIcons style={{margin: "1rem auto"}}>
            <SocialButton
              external
              url="https://www.instagram.com/cocoonvans"
              service="instagram"
              title="Instagram"
              size="small"
              additionalClasses="compact basic"
            />
            <SocialButton
              external
              url="https://www.facebook.com/cocoonvans/"
              service="facebook"
              title="Facebook"
              size="small"
              additionalClasses="compact basic"
            />
            <SocialButton
              external
              url="https://wa.me/0041792698245"
              service="whatsapp"
              title="Whatsapp"
              size="small"
              additionalClasses="compact basic"
            />
            <SocialButton
              url="/contact/"
              service="envelope"
              title="E-Mail"
              size="small"
              additionalClasses="compact basic"
            />
          </SocialIcons>
        </MainContent>
        <AdContainer>
          <AdBlock />
        </AdContainer>
      </Container>
      {/*<Container>
        <InstaGrid
          items={instaPosts}
          columnWidth="33.33%"
          title="Instagram"
        />
      </Container>
      */}
    </Layout>
  )
}

export default Contact

export const query = graphql`
  query {
    allContentfulPost(
      sort: { fields: [publishDate], order: DESC }
      limit: 4
      skip: 0
    ) {
      edges {
        node {
          title
          id
          slug
          publishDate(formatString: "MMMM DD, YYYY")
          heroImage {
            title
            fluid(maxWidth: 1280) {
              ...GatsbyContentfulFluid_withWebp_noBase64
            }
          }
          text {
            json
          }
        }
      }
    }
  }
`
