import React from 'react'
import styled from 'styled-components'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
import config from '../../utils/siteConfig'
import Layout from '../../components/Layout'
import Container from '../../components/Container'
import PageTitle from '../../components/PageTitle'
import PageBody from '../../components/PageBody'
import SEO from '../../components/SEO'

import 'semantic-ui-css/components/segment.min.css'
import 'semantic-ui-css/components/item.min.css'
import 'semantic-ui-css/components/button.min.css'
import 'semantic-ui-css/components/icon.min.css'
import 'semantic-ui-css/components/message.min.css'

const Downloads = ({ data }) => {
  const postNode = {
    title: `Vermietung - ${config.siteTitle}`
  }

  const { text, title, files } = data.contentfulPage

  const Message = styled.p`
    border-radius: 0 !important;
  `

  const SectionContainer = styled.div`
    h1 {
      font-size: 1.5em;
      display: table;
      background: #FEC340;
      color: rgba(0,0,0,0.9);
      padding: 0.3rem;
      border-radius: 2px;
    }
    .ui.basic.button, .ui.basic.buttons .button {
      font-family: 'Roboto', sans-serif;
    }

    .ui.items>.item>.content>a.header {
      font-weight: 400;
      font-size: 1.1em;
      font-family: Roboto, sans-serif;
      line-height: 1.6;
      // margin: 0 0 2em 0;
      border-bottom: 2px solid ${props => props.theme.colors.highlight};
      // font-weight: 700;
      &:hover {
        background: ${props => props.theme.colors.highlight};
        color: ${props => props.theme.colors.base};
      }
    }
}
 `

  return (
    <Layout>
      <Helmet>
        <title>{`Vermietung - ${config.siteTitle}`}</title>
      </Helmet>
      <SEO postNode={postNode} pagePath='contact' customTitle />

      <Container>
        <PageTitle>{title}</PageTitle>
        <PageBody body={text} />

        <div className='ui very basic vertical aligned segment project-downloads'>
          <SectionContainer>
            <h1>Downloads</h1>
            <Message style={{ display: 'none' }} className='ui basic message'>
              Auf die Dateien klicken, um den Download zu starten.
            </Message>
            <div className='ui divided items'>
              {files.map((file, i) => (
                <div className='item' key={`file-${i}`}>
                  <div className='content'>
                    <a
                      className='header'
                      href={file.file.url}
                      target='_blank'
                      rel='noopener noreferrer'
                    >
                      <i
                        className={`icon outline file ${
                          file.file.contentType.split('/')[1]
                        }`}
                      />
                      <h2 style={{ display: 'inline' }}>
                        {file.file.fileName}
                      </h2>
                      <br />
                    </a>
                    <div className='meta'>
                      <a
                        className='header'
                        href={file.file.url}
                        target='_blank'
                        rel='noopener noreferrer'
                      >
                        <button className='ui circular icon button'>
                          <i className='icon download' />
                        </button>
                        <span className='meta'>Datei herunterladen</span>
                      </a>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </SectionContainer>
        </div>
      </Container>
    </Layout>
  )
}

export const query = graphql`
  query {
    contentfulPage(slug: { eq: "downloads" }) {
      id
      title
      text {
        json
      }
      files {
        id
        file {
          fileName
          contentType
          url
        }
      }
    }
  }
`

export default Downloads
