import React from 'react'
import styled from 'styled-components'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
import config from '../../utils/siteConfig'
import Layout from '../../components/Layout'
import Container from '../../components/Container'
import PageTitle from '../../components/PageTitle'
import PageBody from '../../components/PageBody'
import SEO from '../../components/SEO'

import 'semantic-ui-css/components/segment.min.css'
import 'semantic-ui-css/components/item.min.css'
import 'semantic-ui-css/components/button.min.css'
import 'semantic-ui-css/components/icon.min.css'
import 'semantic-ui-css/components/message.min.css'

const Downloads = ({ data }) => {
  const postNode = {
    title: `Vermietung - ${config.siteTitle}`,
  }

  const { text, title } = data.contentfulPage

  const TextContainer = styled.div`
    max-width: ${props => props.theme.sizes.maxWidthCentered};
    margin: 0 auto;
  `

  const Message = styled.p`
    border-radius: 0 !important;
  `

  const SectionContainer = styled.div`
    h1 {
      font-size: 1.5em;
      display: table;
      background: #FEC340;
      color: rgba(0,0,0,0.9);
      padding: 0.3rem;
      border-radius: 2px;
    }
    
    .ui.items>.item>.content>a.header {
      font-weight: 400;
      font-size: 1.1em;
      line-height: 1.6;
      // margin: 0 0 2em 0;
      border-bottom: 2px solid ${props => props.theme.colors.highlight};
      // font-weight: 700;
      &:hover {
        background: ${props => props.theme.colors.highlight};
        color: ${props => props.theme.colors.base};
      }
    }
}
  `

  return (
    <Layout>
      <Helmet>
        <title>{`Vermietung - ${config.siteTitle}`}</title>
      </Helmet>
      <SEO postNode={postNode} pagePath="contact" customTitle />

      <Container>
        <div style={{minHeight: '80vh'}}>
          <PageTitle>{title}</PageTitle>
          <PageBody body={text} />
        </div>
      </Container>
    </Layout>
  )
}

export const query = graphql`
  query {
    contentfulPage(slug: {eq: "kodex"}) {
      id
      title
      text {
        json
      }
    }
  }
`

export default Downloads