import React from 'react'
import styled from 'styled-components'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
import config from '../../utils/siteConfig'
import Layout from '../../components/Layout'
import Container from '../../components/Container'
import PageTitle from '../../components/PageTitle'
import PageBody from '../../components/PageBody'
import SEO from '../../components/SEO'
import CardList from '../../components/CardList'
import Card from '../../components/Card.Rental'

import 'semantic-ui-css/components/segment.min.css'
import 'semantic-ui-css/components/item.min.css'
import 'semantic-ui-css/components/button.min.css'
import 'semantic-ui-css/components/icon.min.css'
import 'semantic-ui-css/components/message.min.css'

const Rental = ({ data }) => {
  const postNode = {
    title: `Vermietung - ${config.siteTitle}`
  }

  const { text, title, files } = data.contentfulPage

  const TextContainer = styled.div`
    max-width: ${props => props.theme.sizes.maxWidthCentered};
    margin: 0 auto;
  `

  const Message = styled.p`
    border-radius: 0 !important;
  `

  const SectionContainer = styled.div`
    h1 {
      font-size: 1.8rem;
      margin: 3rem 0 2rem 0;
      display: table;
      background: ${props => props.theme.colors.highlight};
      color: ${props => props.theme.colors.base};
      padding: 0.3rem;
      border-radius: 2px;
      @media screen and (max-width: 500px) {
        color: ${props => props.theme.colors.highlight};
        background: transparent;
        font-size: 1.25em;
        padding: 0;
      }
    }

    .ui.items > .item > .content > a.header {
      font-weight: 400;
      font-size: 1.1em;
      line-height: 1.6;
      border-bottom: 2px solid ${props => props.theme.colors.highlight};
      &:hover {
        background: ${props => props.theme.colors.highlight};
        color: ${props => props.theme.colors.base};
      }
    }
  `

  return (
    <Layout>
      <Helmet>
        <title>{`Vermietung - ${config.siteTitle}`}</title>
      </Helmet>
      <SEO postNode={postNode} pagePath='contact' customTitle />

      <Container>
        <PageTitle>{title}</PageTitle>
        <PageBody body={text} />

        <TextContainer>
          <SectionContainer>
            <h1>Unsere Miet-Vans</h1>
          </SectionContainer>

          <CardList>
            {data.allContentfulRentalVan.edges.map((el, i) => (
              <Card
                key={el.node.id}
                gallery={el.node.gallery}
                {...el.node}
                featured={i === 0}
              />
            ))}
          </CardList>

          {files && (
            <div className='ui very basic vertical aligned segment project-downloads'>
              <SectionContainer>
                <h1>Downloads</h1>
                <Message
                  style={{ display: 'none' }}
                  className='ui basic message'
                >
                  Auf die Dateien klicken, um den Download zu starten.
                </Message>
                <div className='ui divided items'>
                  {files.map((file, i) => (
                    <div className='item' key={`file-${i}`}>
                      <div className='content'>
                        <a
                          className='header'
                          href={file.file.url}
                          target='_blank'
                          rel='noopener noreferrer'
                        >
                          <i
                            className={`icon outline file ${
                              file.file.contentType.split('/')[1]
                            }`}
                          />
                          <h2 style={{ display: 'inline' }}>{file.title}</h2>
                          <br />
                        </a>
                        <div className='meta'>
                          <a
                            className='header'
                            href={file.file.url}
                            target='_blank'
                            rel='noopener noreferrer'
                          >
                            <button className='ui circular icon button'>
                              <i className='icon download' />
                            </button>
                            <span className='meta'>Datei herunterladen</span>
                          </a>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </SectionContainer>
            </div>
          )}
        </TextContainer>
      </Container>
    </Layout>
  )
}

export const query = graphql`
  query {
    allContentfulRentalVan(
      filter: { verfgbar: { eq: true }, node_locale: { eq: "de-CH" } }
    ) {
      edges {
        node {
          id
          title
          slug
          gallery {
            id
            contentful_id
            title
            file {
              url
            }
            fluid {
              srcSet
              src
              base64
              srcWebp
              srcSetWebp
              ...GatsbyContentfulFluid_withWebp
            }
          }
          description
          calendarID
          text {
            json
          }
          excerpt {
            json
          }
        }
      }
    }
    contentfulPage(slug: { eq: "vermietung" }) {
      id
      title
      text {
        json
      }
      files {
        id
        file {
          url
          fileName
          contentType
        }
        title
        description
      }
    }
  }
`

export default Rental
