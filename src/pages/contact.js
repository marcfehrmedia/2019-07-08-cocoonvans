import React from 'react'
import Helmet from 'react-helmet'
import { graphql } from 'gatsby'
import config from '../utils/siteConfig'
import Layout from '../components/Layout'
import Container from '../components/Container'
import PageTitle from '../components/PageTitle'
import PageBody from '../components/PageBody'
import ContactForm from '../components/ContactForm'
import SEO from '../components/SEO'

const Contact = ({ data }) => {
  const postNode = {
    title: `Kontakt - ${config.siteTitle}`,
  }

  const { text, title } = data.contentfulPage

  return (
    <Layout>
      <Helmet>
        <title>{`Kontakt - ${config.siteTitle}`}</title>
      </Helmet>
      <SEO postNode={postNode} pagePath="contact" customTitle />

      <Container>
        <PageTitle>{title}</PageTitle>
        <PageBody body={text} />
        <ContactForm />
      </Container>
    </Layout>
  )
}

export const query = graphql`
  query {
    contentfulPage(slug: { eq: "contact" }) {
      id
      title
      text {
        json
      }
    }
  }
`

export default Contact
