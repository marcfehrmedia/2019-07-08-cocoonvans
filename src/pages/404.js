import React from 'react'
import { Link } from 'gatsby'
import styled from 'styled-components'
import Helmet from 'react-helmet'
import PageTitle from '../components/PageTitle'
import Container from '../components/Container'
import Layout from '../components/Layout'

const Text = styled.p`
  text-align: center;
  line-height: 1.6;
  a {
    border-bottom: 2px solid ${props => props.theme.colors.highlight};
    font-weight: 700;
    &:hover {
      background: ${props => props.theme.colors.highlight};
      color: ${props => props.theme.colors.base};
    }
  }
`

const NotFoundPage = () => (
  <Layout>
    <Helmet>
      <title>404 - Seite nicht gefunden</title>
      <meta name="description" content="Page not found" />
    </Helmet>

    <Container>
      <PageTitle>Oops..</PageTitle>
      <Text>
        Da lief was schief..
{' '}
        <strong>
          <Link to="/">Hier</Link>
        </strong>
{' '}
        gehts zurück zur Startseite.
      </Text>
    </Container>
  </Layout>
)

export default NotFoundPage
